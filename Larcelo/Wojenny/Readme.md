# Wojenny Mod (War Mod a.k.a. Military Mod) by Larcelo

A Webliero mod with realistic weapons inspired by war games. It has got also pretty cool custom sprites. The biggest feature of this mod is that almost all weapons can destroy the solid rock.

Since the mod was originally created as a Liero 1.36 TC, some things had to be changed to adapt it to Webliero, that is:

1) one weapon was removed (P ATOMOWY - because this weapon caused the game crash due to too many sobjects with high dmg);
2) rope was changed (nrMinLength, nrMaxLength, nrThrowVel);
3) weapon names were translated into English (originallly the names were in Polish).

Anyway, you can find here also the unchanged (original) version of this mod - that's why I uploaded two json files: "wojennyv1.json5" (original version) and "wojennyv2.json5" (webliero fixed version). Both mod versions (wojenny v1 and wojenny v2) are based on the [version uploaded by ChanibaL on Liga Liero Forum.](https://www.liero.org.pl/forum/index.php?action=tpmod;dl=item73) You can watch the original gameplay [here.](https://www.youtube.com/watch?v=aU1EC4oAg7Y)

However, Larcelo has already created further versions of his mod. The latest one is called "Wojenny Mod Revisited" (version 15-11-2021), which you can also find here in this repo (file "wojenny_revisited.json5"). Larcelo made many cool changes in this version, comparing to the first one (including some new weapons added). Wojenny Mod Revisited uses the same sprites as the previous versions of this mod (wojenny v1 and wojenny v2). Enjoy!

Originally, Larcelo created his own custom soundpack for his mod, but hence it's currently impossible to use custom soundpacks in webliero, wojenny v1 and wojenny v2 uses default soundpack from Liero 1.33, and Wojenny Revisited uses csliero soundpack (since it was more suitable for this mod).
