# Liero 1.33 Food Fight mod by Larcelo (version 1.0)

A pretty cool mod with a lot of funny weapons such as potatoes, oysters, plates, french fries, boiling water etc. It has got also very nice custom sprites.

Since the mod was originally created as a Liero 1.33 TC, some things had to be changed to adapt it to Webliero, that is:

1) some weapons were removed (the ones which had "---" in their names - since they were banned anyway in original TC);
2) weapon names were translated into English (originally the names were in Polish).

Anyway, you can find here also the unchanged (original) version of this mod - that's why I uploaded two json files: "foodfightv1.json5" (original version) and "foodfightv2.json5" (webliero fixed version).
